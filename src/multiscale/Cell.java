/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiscale;

/**
 *
 * @author Lighto
 */
class Cell {
    public int x;
    public int y;
    public int seed;
    
    public int energy;
    public boolean recrystalized;
    

    Cell(int randomX, int randomY, int sed) {
        x=randomX;
        y=randomY;
        seed = sed;
    }
    
    Cell(int randomX, int randomY, int sed, boolean rec){
        this(randomX, randomY, sed);
        recrystalized = rec;
    }
    
    Cell(int posX, int posY){
        x = posX;
        y = posY;
    }
}
